import { Injectable } from '@angular/core';
import { Pet } from './pet';
import {Http, RequestOptions, Headers} from '@angular/http';
import 'rxjs/Rx';
import { Observable } from 'rxjs/Observable';


@Injectable()
export class PetService {

  constructor(private http: Http) { }

  API_URL = "https://592382173d82370011e6b624.mockapi.io";


  public getPets(): Observable<Pet[]> {
    return this.http.get(`${this.API_URL}/pets`)
      .map(response => response.json());
  }

  public savePet(name, breed, description) {
    let postHeaders = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: postHeaders });
    let imageUrl = 'http://www.pngall.com/wp-content/uploads/2016/05/Coming-Soon-PNG.png';

    return this.http.post(`${this.API_URL}/pets`, { name, breed, description, imageUrl }, options)
      .map(response => response.json());

  }
}
