import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { PetListComponent } from './pet-list/pet-list.component';
import { PetService } from './pet.service';
import { AdminComponent } from './admin/admin.component';
import { NotFoundComponent } from './not-found/not-found.component';

const routes: Routes = [
  { path: '', component: PetListComponent, pathMatch: 'full'},
  { path: 'admin', component: AdminComponent, pathMatch: 'full'},
  { path: '**', component: NotFoundComponent, pathMatch: 'full'}
]

@NgModule({
  declarations: [
    AppComponent,
    PetListComponent,
    AdminComponent,
    NotFoundComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes)
  ],
  providers: [ PetService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
