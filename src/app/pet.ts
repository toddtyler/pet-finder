export class Pet {
  id?: string;
  name: string;
  imageUrl: string;
  breed: string;
  description: string;
}
