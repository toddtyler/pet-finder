import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { PetService } from '../pet.service';
import { Pet } from '../pet';

@Component({
  selector: 'app-pet-list',
  templateUrl: './pet-list.component.html',
  styleUrls: ['./pet-list.component.css']
})
export class PetListComponent implements OnInit {

  constructor( private petSvc: PetService ) { }

  petData: Pet[];
  @Output() onPetSelect = new EventEmitter<string>();

  ngOnInit() {
     this.petSvc.getPets()
      .subscribe (
        data => {
          this.petData = data;
        },
        error => {
          console.log(error);
        }
      )
  }


  selectPet(pet) {
    this.onPetSelect.emit(pet.name);
  }

}
