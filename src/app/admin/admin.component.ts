import { Component, OnInit } from '@angular/core';
import { PetService } from '../pet.service';
import { Pet } from '../pet';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css']
})
export class AdminComponent implements OnInit {

  constructor( private petSvc: PetService ) { }

  /* pet: Pet = {
    id: '',
    name: '',
    imageUrl: '',
    breed: '',
    description: ''
  }; */

  ngOnInit() {
  }


  onSubmit({ value, valid }: { value: Pet, valid: boolean }) {
    console.log(value, valid);

     this.petSvc.savePet(value.name, value.breed, value.description)
      .subscribe(
        data => {
          //process data returned from server
        },
        error => console.log(error));


  }
}
