import { PetFinderPage } from './app.po';

describe('pet-finder App', () => {
  let page: PetFinderPage;

  beforeEach(() => {
    page = new PetFinderPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
